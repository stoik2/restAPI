import pymysql
from __main__ import app
from config import mysql
from flask import jsonify
from flask import flash, request
import sys, os


@app.route('/api/car', methods=['POST'])
def associate_car():
	try:		
		_json = request.json
		_id_car = _json['id_car']
		_id_customer = _json['id_customer']
		if _id_car and _id_customer and request.method == 'POST':
			conn = mysql.connect()
			cursor = conn.cursor(pymysql.cursors.DictCursor)		

			#Check if car exists
			cursor.execute("SELECT available FROM car WHERE id=%s" % _id_car)
			carRows = cursor.fetchone()
			if carRows is None:
				return "This car doesn't exist", 404
			if carRows['available'] == 0:
				return "This car is not available", 404

			#Check if customer exists
			cursor.execute("SELECT car FROM customer WHERE id=%s" % _id_customer)
			customerRows = cursor.fetchone()
			if customerRows is None:
				return "This cutomer doesn't exist", 404
			if customerRows['car'] and customerRows['car'] != 0:
				return "This customer already has a car", 404

			#Set car as associeted
			sqlQuery = "UPDATE car SET available=%s WHERE id=%s" % (0, _id_car)
			cursor.execute(sqlQuery)
			conn.commit()

			#Set customer for car
			sqlQuery = "UPDATE car SET customer=%s WHERE id=%s" % (_id_customer, _id_car)
			cursor.execute(sqlQuery)
			conn.commit()

			#Add customer's car
			sqlQuery = "UPDATE customer SET car=%s WHERE id=%s" % (_id_car, _id_customer)
			cursor.execute(sqlQuery)
			conn.commit()

			response = jsonify('Car associated successfully!')
			response.status_code = 200
			cursor.close()
			conn.close()
			return response
		else:
			return showMessage(), 404
	except Exception as e:
		return "missing keys", 404


@app.route('/api/car', methods=['GET'])
def car():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT id, available, customer FROM car")
		carRows = cursor.fetchall()
		response = jsonify(carRows)
		response.status_code = 200
		cursor.close()
		conn.close()
		return response
	except Exception as e:
		return str(e), 404

@app.route('/api/car', methods=['PUT'])
def reassociate_car():
	try:
		_json = request.json
		_id_car = _json['id_car']
		_id_customer = _json['id_customer']
		if _id_car and _id_customer and request.method == 'PUT':
			conn = mysql.connect()
			cursor = conn.cursor()

			#Check if car exists
			cursor.execute("SELECT available FROM car WHERE id=%s" % _id_car)
			carRows = cursor.fetchone()
			if carRows is None:
				return "This car doesn't exist", 404
			if carRows == 1:
				return "This car is available", 404

			#Check if customer exists
			cursor.execute("SELECT car FROM customer WHERE id=%s" % _id_customer)
			customerRows = cursor.fetchone()
			if customerRows == None:
				return "This cutomer doesn't exist", 404
			if customerRows and customerRows[0] != None:
				return "This customer already has a car", 404

			#Get car's current customer
			cursor.execute("SELECT customer FROM car WHERE id=%s" % _id_car)
			currentCustomer = cursor.fetchone()
			if currentCustomer is not None:
				#Deassociate car's current customer
				sqlQuery = "UPDATE customer SET car=%s WHERE id=%s"
				bindData = (None, currentCustomer[0])
				cursor.execute(sqlQuery, bindData)
				conn.commit()

			#Associate car to customer
			sqlQuery = "UPDATE car SET customer=%s WHERE id=%s" 
			bindData = (_id_customer, _id_car)
			cursor.execute(sqlQuery, bindData)
			conn.commit()

			#Add customer's car
			sqlQuery = "UPDATE customer SET car=%s WHERE id=%s" % (_id_car, _id_customer)
			cursor.execute(sqlQuery)
			conn.commit()

			response = jsonify('Car reassociated successfully!')
			response.status_code = 200
			cursor.close()
			conn.close()
			return response
		else:
			return showMessage()
	except Exception as e:
		return str(e), 404


@app.route('/api/car', methods=['DELETE'])
def disassociate_car():
	try:
		_json = request.json
		_id = _json['id']
		if _id and request.method == 'DELETE':
			conn = mysql.connect()
			cursor = conn.cursor()

			#Check if car exists
			cursor.execute("SELECT available FROM car WHERE id=%s" % _id)
			carRows = cursor.fetchone()
			if carRows is None:
				return "This car doesn't exist", 404

			#Free car
			sqlQuery = "UPDATE car SET available=%s, customer=%s WHERE id=%s"
			bindData = (1, None, _id)
			cursor.execute(sqlQuery, bindData)
			conn.commit()

			#Remove customer's car
			sqlQuery = "UPDATE customer SET car=%s WHERE car=%s"
			bindData = (None, _id)
			cursor.execute(sqlQuery, bindData)
			conn.commit()

			response = jsonify('Car disassociated successfully!')
			response.status_code = 200
			cursor.close()
			conn.close()
			return response
		else:
			return showMessage(), 404
	except Exception as e:
		return "missing keys", 404
