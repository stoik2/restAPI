# RESTful API

# Backend Software Engineer

We are a car leasing company starting a business with 10 cars.
You need to create a REST API that will:
-   handle customers
-   associate and disassociate a car to a customer if possible

The API should be dockerized. In production, the service will be replicated (three replicas).
The code should be the cleanest possible.

Stack: Go ou python (flask or fastAPI), Dockerfile, sql, test

## Architecture
```
── restAPI
    ├── app
    │   ├── app.py
    │   ├── car.py
    │   ├── config.py
    │   ├── customer.py
    │   ├── Dockerfile
    │   └── requirements.txt
    ├── db
    │   └── init.sql
    ├── docker-compose.yml
    ├── README.md
    └── tests
        ├── app_test
        │   ├── app.py
        │   ├── car.py
        │   ├── config.py
        │   ├── customer.py
        │   ├── Dockerfile
        │   └── requirements.txt
        ├── db_test
        │   └── init.sql
        ├── docker-compose.yml
        └── test_restapi.py
```

## REST API

### Start the API
```
$> cd restAPI
$> docker compose up
restapi-app-1  |  * Running on http://172.18.0.3:5000 (Press CTRL+C to quit)
```
### Customer methods

| Method | Arguments | Description | Example |
| ----------- | ----------- | ----------- | ----------- |
| GET | None | List all customers | ```curl http://172.18.0.3:5000/api/customer ```
| POST | name | Create a customer | ```curl -X POST  -H 'Content-Type: application/json' -d '{"name":"test"}' http://172.18.0.3:5000/api/customer ```
| PUT | id, name | Change customer's name | ```curl -X PUT  -H 'Content-Type: application/json' -d '{"id":1, "name":"test2"}' http://172.18.0.3:5000/api/customer ```
| DELETE | id | Delete customer | ```curl -X DELETE  -H 'Content-Type: application/json' -d '{"id":"1"}' http://172.18.0.3:5000/api/customer ```

### Car methods

| Method | Arguments | Description | Example |
| ----------- | ----------- | ----------- | ----------- |
| GET | None | List all cars | ```curl http://172.18.0.3:5000/api/car ```
| POST | id_car, id_customer | Associate car | ```curl -X POST  -H 'Content-Type: application/json' -d '{"id_car":1, "id_customer":1}' http://172.18.0.3:5000/api/car ```
| PUT | id_car, name | Re-associate car | ```curl -X PUT  -H 'Content-Type: application/json' -d '{"id_car":1, "id_customer":"2"}' http://172.18.0.3:5000/api/car ```
| DELETE | id | Disassociate car | ```curl -X DELETE -H 'Content-Type: application/json' -d '{"id":"1"}' http://172.18.0.3:5000/api/car ```



## Tests

Requirements:
```pip3 install -U pytest```

Run tests:
```
$> cd tests
$> docker compose up
$> pytest test_restapi.py
```

