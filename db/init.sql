CREATE DATABASE restAPI;
use restAPI;

CREATE TABLE customer (
  id int NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  car int,
  PRIMARY KEY (id)
);

CREATE TABLE car (
  id int NOT NULL AUTO_INCREMENT,
  available boolean NOT NULL,
  customer int,
  PRIMARY KEY (id)
);


INSERT INTO car
  (available)
VALUES
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1'),
  ('1');
