import requests
import json

def test_get_customers():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	assert response.status_code == 200

def test_create_customers():
	response = requests.post("http://127.0.0.1:50011/api/customer", json={"name": "test"})
	assert response.status_code == 200

def test_put_customer():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.put("http://127.0.0.1:50011/api/customer", json={"id": elem["id"], "name": "test"})
			assert response.status_code == 200

def test_put_non_existing_customer():
	response = requests.put("http://127.0.0.1:50011/api/customer", json={"id": -1, "name": "test"})
	assert response.status_code == 404

def test_delete_customer():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.delete("http://127.0.0.1:50011/api/customer", json={"id": elem["id"]})
			assert response.status_code == 200

def test_delete_non_existing_customer():
	response = requests.delete("http://127.0.0.1:50011/api/customer", json={"id": -1})
	assert response.status_code == 404


def test_get_cars():
	response = requests.get("http://127.0.0.1:50011/api/car")
	assert response.status_code == 200

def test_associate_cars():
	response = requests.post("http://127.0.0.1:50011/api/customer", json={"name": "test"})
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": 1, "id_customer": elem["id"]})
			assert response.status_code == 200

def test_associate_non_existing_car():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": -1, "id_customer": elem["id"]})
			assert response.status_code == 404

def test_associate_non_existing_customer():
	response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": 1, "id_customer": -1})
	assert response.status_code == 404

def test_diassociate_car():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": 1, "id_customer": elem["id"]})
			response = requests.delete("http://127.0.0.1:50011/api/car", json={"id": 1})
			assert response.status_code == 200

def test_associate_already_associated_car():
	response = requests.get("http://127.0.0.1:50011/api/customer")
	for elem in response.json():
		if elem["name"] == "test":
			response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": 1, "id_customer": elem["id"]})
			response = requests.post("http://127.0.0.1:50011/api/car", json={"id_car": 1, "id_customer": elem["id"]})
			assert response.status_code == 404
			response = requests.delete("http://127.0.0.1:50011/api/customer", json={"id": elem["id"]})
