from __main__ import app
from flaskext.mysql import MySQL

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'toor'
app.config['MYSQL_DATABASE_DB'] = 'restAPI'
app.config['MYSQL_DATABASE_HOST'] = 'db'
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
mysql.init_app(app)
