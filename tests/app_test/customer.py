import pymysql
from __main__ import app
from config import mysql
from flask import jsonify
from flask import flash, request


@app.route('/api/customer', methods=['POST'])
def create_customer():
	try:		
		_json = request.json
		_name = _json['name']
		if _name and request.method == 'POST':
			conn = mysql.connect()
			cursor = conn.cursor(pymysql.cursors.DictCursor)		
			sqlQuery = "INSERT INTO customer(name) VALUES(%s)"
			bindData = (_name)			
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			response = jsonify('Customer added successfully!')
			response.status_code = 200
			return response
		else:
			return 'Key "name" not found', 404
	except Exception as e:
		return str(e), 404


@app.route('/api/customer', methods=['GET'])
def customer():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT id, name, car FROM customer")
		customerRows = cursor.fetchall()
		response = jsonify(customerRows)
		response.status_code = 200
		return response
	except Exception as e:
		print(e)


@app.route('/api/customer', methods=['PUT'])
def update_customer():
	try:
		_json = request.json
		_id = _json['id']
		_name = _json['name']
		if _name and _id and request.method == 'PUT':
			conn = mysql.connect()
			cursor = conn.cursor()

			#Check if customer exists
			cursor.execute("SELECT name FROM customer WHERE id=%s" % _id)
			customerRows = cursor.fetchone()
			if customerRows is None:
				return "This customer doesn't exist", 404

			#Change customer's name
			sqlQuery = "UPDATE customer SET name=%s WHERE id=%s"
			bindData = (_name, _id,)
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			response = jsonify('Customer updated successfully!')
			response.status_code = 200
			return response
		else:
			return showMessage(), 404
	except Exception as e:
		return 'Keys not found', 404


@app.route('/api/customer', methods=['DELETE'])
def delete_customer():
	try:
		_json = request.json
		_id = _json['id']
		if _id and request.method == 'DELETE':
			conn = mysql.connect()
			cursor = conn.cursor()

			#Check if customer exists
			cursor.execute("SELECT car FROM customer WHERE id=%s" % _id)
			customerRows = cursor.fetchone()
			if customerRows is None:
				return "This customer doesn't exist", 404

			#Check if customer has a car
			if customerRows[0]:
				sqlQuery = "UPDATE car SET available=%s, customer=%s WHERE id=%s"
				bindData = (1, None, customerRows[0])
				cursor.execute(sqlQuery, bindData)
				conn.commit()

			#Delete customer
			sqlQuery = "DELETE FROM customer WHERE id=%s"
			bindData = (_id,)
			cursor.execute(sqlQuery, bindData)
			conn.commit()
			response = jsonify('Customer deleted successfully!')
			response.status_code = 200
			return response
		else:
			return showMessage()
	except Exception as e:
		return 'Keys not found', 404
