from flask import Flask
from flask_cors import CORS, cross_origin
from flask import jsonify, flash, request


app = Flask(__name__)
CORS(app)

import pymysql
from config import mysql
import customer
import car


@app.errorhandler(404)
def showMessage(error=None):
	message = {
		'status': 404,
		'message': 'Record not found: ' + request.url,
	}
	respone = jsonify(message)
	respone.status_code = 404
	return respone


if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
